#!/bin/bash
echo 'Starting Scan !'
echo "File Name: $1"
echo "File Path: $2"
echo "File Type: $3"
echo "Output Path: $4"
# read -p "Enter Path to file: " filepath
# read -p "Enter File type: " filetype #docker / helm etc
finalpath="${2}/${1}"
mkdir -p "/app\iac\results"
echo """terrascan scan -f $finalpath -i $3 -x json -o json > ""${2}/${4}/${1}_${date +"%Y%m%dT%H%M"}_result.json"""
terrascan scan -f $finalpath -i $3 -x json -o json > "${2}/${4}/${1}_${date +"%Y%m%dT%H%M"}_result.json" 
echo "Scanning Completed"